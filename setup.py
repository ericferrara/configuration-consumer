import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='configuration-consumer',
    version='0.0.1',
    author='Eric Ferrara',
    author_email='ericferrara@gmail.com',
    description='Generic configuration reader with support for required fields',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='http://www.ericferrara.com',
    package_dir={'': './src'},
    packages=setuptools.find_packages(where='./src'),
    classifiers=[
        'Programming Language :: Python :: 3'
    ],
    python_requires='>=3.6',
    requires=['pyYAML', 'tempfile']
)
