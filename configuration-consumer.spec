%define name configuration-consumer
%define version 0.0.1
%define unmangled_version 0.0.1

Summary: Generic configuration reader with support for default values and required attributes	
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{unmangled_version}.tar.gz
Group: Development/Libraries
License: GNU General Public License
URL: https://gitlab.com/ericferrara/configuration-consumer 
Source0:	
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
BuildRequires: python36-setuptools rpm-build
Requires: python36-pyYAML

%description
Generic configuration reader with support for default values and required attributes


%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{unmangled_version}


%build
python3 setup.py build


%install
python3 setup.py install --single-version-externally-managed -01 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES
xargs dirname < INSTALLED_FILES | sort | uniq > INSTALLED_DIRS
cat INSTALLED_DIRS >> INSTALLED_FILES

%files - INSTALLED_FILES
%defattr(-,root,root)

%clean 
rm -rf $RPM_BUILD_ROOT
