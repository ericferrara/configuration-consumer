import pprint
import sys
import tempfile
import yaml

class ConfigurationConsumer(object):

    def __init__(self, configuration_file=None, default_attrs={}, required_attrs=[]):
        
        self.default_attributes = default_attrs
        self.required_attributes = required_attrs

        if not configuration_file:
            print("Error: No configuration file provided to 'ConfigurationConsumer' object.")
            sys.exit()

        self.config = self.read_config(configuration_file)

    def get_config(self):
        return self.config

    def check_req(self, required_attribute, tmpconf):
        if required_attribute not in tmpconf:
            raise ValueError("Configuration file missing required attribute '{}'. Please check configuration.".format(required_attribute))
            return False
        return True

    def check_reqs(self, required_attribute, tmpconf):

        # - if attribute is a set, convert it to a list for easy use
        if isinstance(required_attribute, set):
            required_attribute = list(required_attribute)

        # - if attribute is a string, we can use it as-is
        if isinstance(required_attribute, str):
            if not self.check_req(required_attribute, tmpconf):
                return False
        # - if attribute is a dict, descend into it to get nested configuration values
        elif isinstance(required_attribute, dict):
            for sub_required_attribute in required_attribute.keys():
                if not self.check_req(sub_required_attribute, tmpconf):
                    return False
                else: 
                    self.check_reqs(required_attribute[sub_required_attribute], tmpconf[sub_required_attribute])
        # - if attribute is a list, descend one level to grab list values
        elif isinstance(required_attribute, list):
            for sub_required_attribute in required_attribute:
                if not self.check_req(sub_required_attribute, tmpconf):
                    return False
        # - in any other case, fail because we don't recognize the type of the configuration item            
        else:
            raise ValueError('Unrecognized structure in yaml')
            return False

        return True

    def read_config(self, configuration_file=None):
        
        # - check if file exists, and is readable as a yaml file by ConfigurationConsumer
        try: 
            with open(configuration_file, 'r') as stream:
                try:
                    config = {**self.default_attributes, **yaml.safe_load(stream)}
                except yaml.YAMLError as e:
                    raise
                    raise ValueError("Error: Could not parse YAML in file '{}'".format(config_file))
                    return False
        except IOError as e:
            raise OSError("Error: Could not read configuration file '{}'".format(config_file))
            return False

        # - look for required configuration iteams
        for required_attribute in self.required_attributes:
            if not self.check_reqs(required_attribute, config):
                return False
        return config

# - for testing, only
if __name__ == "__main__":
    test_config = {
        'a': 'test1',
        'b': 'test2',
        'ab': 'test-mod-overwrite',
        'c': ['one', 'two', 'three'],
        'req_field': {'present': 'xa', 'present2': 'xb'},
        'd': {'x': 'foo', 'y': 'bar'}
    }

    x = None
    tempf = tempfile.NamedTemporaryFile(suffix='.yml', mode='r+')
    tempf.write(yaml.dump(test_config, default_flow_style=False))
    tempf.seek(0)
    cfg = ConfigurationConsumer(
        configuration_file = tempf.name,
        default_attrs={'ab': 'test-mod'},
        required_attrs=[{'req_field': ['present', 'present2']}]
    )
    pprint.pprint(cfg.get_config())

